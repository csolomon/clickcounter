//
//  ViewController.swift
//  clickcounter
//
//  Created by Chris Solomon on 2018-10-20.
//  Copyright © 2018 Chris Solomon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	var count = 0
	var doubleCount = 0
	
	var label: UILabel!
	var additionalLabel: UILabel!
	
	enum BackgroundColor {
		case red(UIColor)
		case green(UIColor)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = UIColor.green

		// label
		let label = UILabel()
		label.frame = CGRect(x: 150, y: 150, width: 60, height: 60)
		label.text = "0"
		self.label = label

		view.addSubview(label)

		// additional label
		let additionalLabel = UILabel()
		additionalLabel.frame = CGRect(x: 250, y: 150, width: 60, height: 60)
		additionalLabel.text = "0"
		self.additionalLabel = additionalLabel

		view.addSubview(additionalLabel)

		// Increment button
		let incrementButton = UIButton()
		incrementButton.frame = CGRect(x: 150, y: 250, width: 60, height: 60)
		incrementButton.setTitle("More", for: .normal)
		incrementButton.setTitleColor(UIColor.blue, for: .normal)
		
		view.addSubview(incrementButton)
		incrementButton.addTarget(self, action: #selector(ViewController.incrementCount), for: UIControl.Event.touchUpInside)

		// Increment button
		let decrementButton = UIButton()
		decrementButton.frame = CGRect(x: 250, y: 250, width: 60, height: 60)
		decrementButton.setTitle("Less", for: .normal)
		decrementButton.setTitleColor(UIColor.blue, for: .normal)
		
		view.addSubview(decrementButton)
		decrementButton.addTarget(self, action: #selector(ViewController.decrementCount), for: UIControl.Event.touchUpInside)
	}

	@objc func incrementCount()
	{
		self.count += 1
		self.doubleCount = self.count * 2
		
		self.label.text = "\(self.count)"
		self.additionalLabel.text = "\(self.doubleCount)"
		
		toggleBackground()
	}
	
	@objc func decrementCount()
	{
		self.count -= 1
		self.doubleCount = self.count * 2
		
		self.label.text = "\(self.count)"
		self.additionalLabel.text = "\(self.doubleCount)"
		
		toggleBackground()
	}
	
	func toggleBackground()
	{
//		self.count % 2 == 0 ? view.backgroundColor = UIColor.green : view.backgroundColor = UIColor.red
		if self.count % 2 == 0
		{
			view.backgroundColor = UIColor.green
		}
		else
		{
			view.backgroundColor = UIColor.red
		}
	}
	
}

